# novel

- title: さようなら竜生、こんにちは人生
- title_zh1: 再見龍生你好人生
- author: 永岛ひろあき
- illust: 市丸きすけ
- source: https://www.alphapolis.co.jp/novel/675104537/65069615
- cover: https://cache2-ebookjapan.akamaized.net/contents/thumb/l/M3100029985361.jpg?20180926115013
- publisher: alphapolis
- date: 2019-08-13T18:36:00+08:00
- status: 连载中
- novel_status: 0x0300

## illusts

- くろの

## publishers

- wenku8

## series

- name: 再见龙生你好人生

## preface


```
　最強最古的龍，厭倦了漫長的生命，故意死在來討伐自己的勇者們的手上。
　龍本以為這下就可以去到冥府永眠了，但回過神來，卻轉生成了人類的小孩。
　由龍變為人類，重新體會到活著的快樂的龍，決定作為人繼續生活下去。
　生為邊境農民孩子的龍，隱藏著靈魂中蘊含的偉大力量過著平淡的生活，但他遇見拉米亞族的少女、黑薔薇的妖精後，因對魔法的力量產生了興趣而去魔法學院上學。
　曾經是龍的這個人類，在魔法學院的生活中，與美麗而又強大的同學們，大地母神和吸血鬼女王、龍族女皇們成為了朋友，體會到了活著的喜悅與幸福。
```

## tags

- node-novel
- alphapolis
- ファンタジー
- wenku8
- 最強
- 勇者
- 竜
- 神
- 魔法
- 

# contribute

- KLinys丶孤
- 小墨
- Sophia__lian
- Aulstyne
- 好船子
- 漠言
- 血色诗人
- 莫语
- keroneo
- 0RS
- 罗斯西瓜
- 无聊
- 再见龙生应援群
- I法克灬油
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: false

## wenku8

- novel_id: 2224

# link

- [再见龙生你好人生吧](https://tieba.baidu.com/f?kw=%E5%86%8D%E8%A7%81%E9%BE%99%E7%94%9F%E4%BD%A0%E5%A5%BD%E4%BA%BA%E7%94%9F&ie=utf-8&tp=0 "再见龙生你好人生")
- https://narou.nar.jp/search.php?text=%E3%81%95%E3%82%88%E3%81%86%E3%81%AA%E3%82%89%E7%AB%9C%E7%94%9F&novel=all&genre=all&new_genre=all&length=0&down=0&up=100
- 再见龙生应援群：786920501
- https://pan.baidu.com/s/1sknKB02uaFg82m8HdUPYgA 0gru
- https://pan.baidu.com/s/1e1b2p1ohMyExr3tYL0CWSA tg96
- http://www.dm5.com/manhua-zaijianlongshengnihaorensheng/
- https://pan.baidu.com/s/18PuBqLsYkUbcoGtPAWjJag wtjv
- [文库EPUB及生肉整合](https://tieba.baidu.com/p/5646717436 "文库EPUB及生肉整合")
- http://www.wenku8.com/book/2224.htm
- 

