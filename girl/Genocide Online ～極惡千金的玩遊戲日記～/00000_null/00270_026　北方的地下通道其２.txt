作者的話：
成為按類型排行的週間第１位了，非常感謝！這也是各位不吝的評價所贈予的禮物，的確由於再多寫一些的話我就會被擔憂著，所以這次就沒有連續更新了，但真的非常感謝各位，以後還請各位繼續多多指教。

麗奈的新的被害者⋯⋯


════════════════════

「原來如此～」

登入後從BOSS的房間前進了一段時間。儘管海水不知從何處漏了進來，為甚麼這裡沒有被水淹沒呢？因為這是一個真實得可怕的遊戲，我希望調查出這個現象的成因，原來似乎是海水一旦累積起來後就會變成那些藍色水晶。
因此在最下層遇見BOSS的房間裡，是那樣的光輝燦爛⋯⋯⋯

「⋯⋯好的，到這樣就可以了」

我向遇見的玩家威脅⋯⋯⋯⋯提出了這個問題，然後聽到了解答。看來他是非常適合做驗證班的人呢。

「為甚麼，屠殺者會在這裡⋯⋯不，雖然也沒甚麼好奇怪的⋯⋯」
「⋯⋯有甚麼問題嗎？」
「────咿！沒、沒有！甚麼問題都沒有！！」

不知為何他在小聲嘟嚷著，於是就向他提問了，但他古怪地變得拘謹起來。話說屠殺者是誰來著？是不是將我跟其他人認錯了呢？

「⋯⋯那個？差不多請放我走了吧」
「因為很方便所以請跟我一起行動」
「啊，是的」

似乎他知道各種各樣詳細的資訊呢，就借此機會逼他將調查過的事一五一十地說出來吧。
因為我根本完全沒有跟其他玩家交流，這個機會很寶貴哦。


▼▼▼▼▼▼▼

「上述就是所有關於現在前往的『貝爾森斯托赫市』的情報了⋯⋯⋯⋯」
「原來如此，謝謝你」

現在的目的地『貝爾森斯托赫市』與『起始之城』不同，那裡有數個海灣成為了港口，是一個利用海路使得貿易繁盛並且洋溢著各式各樣商品的航運城市。
各種商品從世界各地聚集過來，即使是人亦被視為商品，可以說這正加劇了它作為一個奴隸城市的程度。

「奴隸支撐著市民的生活嗎⋯⋯」
「嗯，就是這樣。搬運貨物的體力勞動和清掃下水道等都需要他們，而沒有人想做的工作亦全都交給奴隸去做了」

似乎在奴隸的事上不存在合法或非法的問題，而奴隸反抗主人亦是司空見慣的事。自從現屆領主上任後，對奴隸的壓迫變得更強了，此時已然談不上對最低限度的衣食住的保障，而是奴隸的性命堪虞，聽說目前已經是『如果死了的話再買一個不就好了嗎』的境地了。
然而，正正由於將奴隸作為勞動力進行剝削，城市越發繁榮，看來中產階級以上的富裕階層對此寄予了深厚的支持。

「⋯⋯那個，屠殺者」
「⋯⋯那個，是對我的稱呼嗎？」

從剛才開始我就在想那到底是誰，看來像是在叫我呢？

「呃，你不知道嗎？你可是玩家之間的名人哦？！」
「⋯⋯是這樣嗎，但是我的名稱並不是屠殺者」

為甚麼叫屠殺者⋯⋯⋯⋯嘛，大致上猜到了，怎麼想都是因為我在城裡進行PK的事吧。

「那，該怎叫你⋯⋯」
「我的名字是麗奈，請如此稱呼我」
「誒？！就是那個第一隊攻略地下墳墓的隊伍的胡鬧名字────」
「────舌頭和喉嚨，你想哪樣被切開？」

因為聽到了無法置若罔聞的粗暴說話，我將短刀放進他的口裡，靠近他的臉並逼他做出選擇。

「⋯⋯⋯⋯呼嘻嘛呼誒誒嘶嘻」
「⋯⋯嘛，這次就算了吧」

因為至今為止告訴了我不少的事情，這次就放過他吧⋯⋯⋯⋯⋯

「咕噗咕噗⋯⋯好、好可怕⋯⋯⋯⋯」

這個人每次都這麼誇張呢？雖然看著很有趣，但每一次都要這樣嗎？

「⋯⋯話說，麗奈？之前是不是在哪裡聽過？」
「？第一次攻略的隊伍名稱？」
「啊，不是這個，我是說現實中⋯⋯」
「這也不是甚麼罕見的名字，有甚麼令你介意的事嗎？」
「⋯⋯⋯⋯⋯⋯仔細一看，好像在哪裡見過麗奈的樣子呢？（嘟嚷）」

他在小聲嘟嚷著，似乎在沉思著甚麼事情⋯⋯⋯⋯雖然完全聽不到說話的內容，但我對此不感興趣，所以就置之不理了。

「比起這個，還是快點繼續前進吧？」
「啊，等一下我！如果我被留在這樣的深處我會死的！還有我叫優！我還沒有說過吧？」
「是這樣嗎，那麼走吧」

在慌張地跟在後面的他的陪伴之下，繼續前進。


▼▼▼▼▼▼▼

作者的話：
優君，你持有一大堆初見殺的技能，認真地逃的話是有可能逃走的⋯⋯⋯⋯只是，一見面就抓住衣領、拉倒在地上、將短刀放進口裡進行威脅的麗奈是異常的⋯⋯⋯⋯一般來說是不可能在剎那間作出反應的⋯⋯⋯⋯⋯⋯⋯